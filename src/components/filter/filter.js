import React from 'react';
import PropTypes from 'prop-types';
import './filter.scss';

const filter = (props) => {
  let filterNames = props.filterNames;
  const handleKeyDown = (e,filter) => {
    if (e.key === 'Enter') {
      props.onFilter(filter,e.target.value);
    }
  }
  return (
    <div className="filter">
      {
        filterNames.map((filter,index) =>
          <div className='filter-option' key={index}>
            <label htmlFor={filter}>{filter}</label>
            <input type='text' id={filter} onKeyDown={(e) => handleKeyDown(e,index) }></input>
          </div>
        )
      }
    </div>
  )
};

filter.propTypes = {
  filterNames: PropTypes.array
};

filter.defaultProps = {
  filterNames: ['نام تغییر دهنده','تاریخ','نام آگهی','فیلد']
};

export default filter;
