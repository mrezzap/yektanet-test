import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './table.scss';

function Table(props) {
  const [isMobile, setIsMobile] = useState(false);
  const ids = props.ids;
  let headers = props.headers;
  let data = props.data;
  localStorage.setItem('ids', JSON.stringify(ids));
  console.log(props.savedData);
  const sd = props.savedData;
    let tmp = [];
    sd.forEach(element => {
      let idx = data.findIndex(i => i.id === element.id);
      console.log(idx);
      if (idx < 0) {
        console.log('i am hereeee')
        tmp.push(element);
      } else {
        data.sort(function(x,y){ return x == element ? -1 : y == element ? 1 : 0; });
      }
    });
    data = [...tmp, ...data];
    console.log(sd,'tmp:', tmp, 'data:', data);
  const onHandleResize = () => {
    setIsMobile(window.matchMedia("(max-width: 600px)").matches ? true : false);
    console.log(window.matchMedia("(max-width: 600px)").matches ? true : false);
  }
  useEffect(() => {
    setIsMobile(window.matchMedia("(max-width: 600px)").matches ? true : false);
    window.addEventListener('resize', onHandleResize.bind(this));
    return function cleanup() {
      window.removeEventListener('resize', onHandleResize.bind(this));
    }
  }, []);
  
  const headerClicked = (index) => {
    props.onSort(index);
  };

  const rowSelectHandler = (id) => {
    props.onRowSelect(id);
  };
  return (
    <div className="table">
      {!isMobile && <div className='table-wrapper'>
        {headers.map((header, index) =>
          <div key={index} className='table-cell table-header' onClick={() => headerClicked(index)}>{header}</div>
        )}
        {data.map((data, index) =>
          <div key={index} className={`cell-wrapper ${ids.includes(data.id) ? "selcted-row" : ""}`} onClick={() => rowSelectHandler(data.id)}>
            <div className='table-cell'>{data.name}</div>
            <div className='table-cell'>{data.date}</div>
            <div className='table-cell'>{data.title}</div>
            <div className='table-cell'>{data.field}</div>
            <div className='table-cell'>{data.old_value}</div>
            <div className='table-cell'>{data.new_value}</div>
          </div>
        )}
      </div>}
      {isMobile &&
        data.map((data, index) =>
          <div key={index} className='table-wrapper-responsive' onClick={() => rowSelectHandler(data.id)}>
            <div className='header-wrapper'>
              {headers.map((header, index) =>
                <div key={index} className='table-header' onClick={() => headerClicked(index)}>{header}</div>
              )}
            </div>
            <div className='cell-wrapper'>
              <div className='table-cell'>{data.name}</div>
              <div className='table-cell'>{data.date}</div>
              <div className='table-cell'>{data.title}</div>
              <div className='table-cell'>{data.field}</div>
              <div className='table-cell'>{data.old_value}</div>
              <div className='table-cell'>{data.new_value}</div>
            </div>
          </div>
        )
      }
    </div>
  );
};

Table.propTypes = {
  headers: PropTypes.array,
  data: PropTypes.array,
  ids: PropTypes.array
};

Table.defaultProps = {
  headers: ['نام تغییر دهنده', 'تاریخ', 'نام آگهی', 'فیلد', 'مقدار قدیمی', 'مقدار جدید'],
  data: [],
  ids: []
};

export default Table;
