import './App.scss';
import Filter from './components/filter/filter';
import Table from './components/table/table';
import data from './assets/data.json';
import { useEffect, useState } from 'react';
import Pagination from './components/pagination/pagination';


function App() {
  const [params, setParams] = useState(new URLSearchParams(window.location.search))
  const [testData, setTestData] = useState(data);
  const [pageNumber, setPageNumber] = useState(0);
  const [rowNumber, setRowNumber] = useState(25);
  const [ids, setIds] = useState(localStorage.getItem('ids') ? JSON.parse(localStorage.getItem('ids')): []);
  const [savedData,setSavedData] = useState([]);
  useEffect(() => {
    if(localStorage.getItem('ids')) {
      let arr = JSON.parse(localStorage.getItem('ids'));
      if (arr.length > 0) {
        let tmp = [];
        arr.forEach(element => {
          let idx = tmp.findIndex(i => i.id === element);
          if (idx < 0) {
            let ele = data.find(i => i.id === element);
            tmp.push(ele);
          }
        });
        setSavedData(tmp);
      }
    }
    filter();
  }, [])
  const setQueryParams = (field, value) => {
    switch (field) {
      case 0: {
        params.set('name', value);
        break;
      }
      case 1: {
        params.set('date', value);
        break;
      }
      case 2: {
        params.set('title', value);
        break;
      }
      case 3: {
        params.set('field', value);
        break;
      }
      default: {
        break;
      }
    }
    window.history.pushState(null, null, "?" + params.toString());
    setParams(new URLSearchParams(window.location.search));
    filter();
  };
  const setSortQueryParam = (value) => {
    switch (value) {
      case 0: {
        params.set('sort', 'name');
        break;
      }
      case 1: {
        params.set('sort', 'date');
        break;
      }
      case 2: {
        params.set('sort', 'title');
        break;
      }
      case 3: {
        params.set('sort', 'field');
        break;
      }
      case 4: {
        params.set('sort', 'old_value');
        break;
      }
      case 5: {
        params.set('sort', 'new_value');
        break;
      }
      default: {
        break;
      }
    }
    window.history.pushState(null, null, "?" + params.toString());
    setParams(new URLSearchParams(window.location.search));
    sort();
  };
  const filter = () => {
    let newData = data.filter(data => {
      const hasName = params.get('name') ? data.name.toLowerCase().indexOf(params.get('name').toLowerCase()) > -1 : true;
      // const hasDate = params.get('date') ? data.date.toLowerCase().indexOf(params.get('date').toLowerCase()) > -1 : true;
      const hasTitle = params.get('title') ? data.title.toLowerCase().indexOf(params.get('title').toLowerCase()) > -1 : true;
      const hasField = params.get('field') ? data.field.toLowerCase().indexOf(params.get('field').toLowerCase()) > -1 : true;
      return hasName && hasTitle && hasField;
    });
    const hasSort = params.get('sort') ? true : false;
    const hasDate = params.get('date') ? true : false;
    if (hasDate) {
      console.log(newData);
      findDate(params.get('date'),newData);
    } else {
      if (hasSort) {
        sort(newData);
      } else {
        setTestData(
          newData
        );
      }
    }
    console.log(newData, testData);
  };

  const sort = (newData) => {
    console.log(params.get('sort'), window.location.search);
    const hasSort = params.get('sort') ? true : false;
    let sort = params.get('sort');
    if (hasSort) {
      switch (sort) {
        case 'name': {
          newData ?
            setTestData(sortFunc('name', newData)) : setTestData(sortFunc('name', testData))
          break;
        }
        case 'date': {
          newData ? setTestData(sortDateFunc(newData)) : setTestData(sortDateFunc(testData))
          break;
        }
        case 'title': {
          newData ?
            setTestData(sortFunc('title', newData)) : setTestData(sortFunc('title', testData));
          break;
        }
        case 'field': {
          newData ?
            setTestData(sortFunc('field', newData)) : setTestData(sortFunc('field', testData));
          break;
        }
        default: {
          break;
        }
      }
      console.log(testData);

    }
  }

  const sortFunc = (field, data) => {
    return data.sort((a, b) => {
      var nameA = a[field].toLowerCase();
      var nameB = b[field].toLowerCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    })
  };

  const sortDateFunc = (data) => {
    return data.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    })
  }

  const comparefunc = (a, b) => {
    return new Date(b) - new Date(a);
  };

  const findDate = (value,data) => {
    let d = sortDateFunc(data);
    setTimeout(() => {
      let result = binarySearch(d, value, comparefunc)
      let first = result;
      let last = result;
      if (result > 0) {
        while (first > 0 && d[first - 1].date == value)
          first--;
        while (last < d.length - 1 && d[last + 1].date == value)
          last++;
      }
      const hasSort = params.get('sort') ? true : false;
      if (hasSort) {
        sort(d.slice(first,last + 1));
      } else {
        setTestData(
          d.slice(first,last + 1)
        );
      }
      console.log(value, result, first, last,d);
    }, 1000)
  }

  const binarySearch = (ar, el, compare_fn) => {
    var m = 0;
    var n = ar.length - 1;
    while (m <= n) {
      var k = (n + m) >> 1;
      var cmp = compare_fn(el, ar[k].date);
      console.log(cmp);
      if (cmp > 0) {
        m = k + 1;
      } else if (cmp < 0) {
        n = k - 1;
      } else {
        return k;
      }
    }
    return -m - 1;
  }

  const pageNumberHandler = (value) => {
    console.log(value);
    setPageNumber(value);
  }
  const rowNumberHandler = (value) => {
    console.log(value);
    setRowNumber(value);
  }
  const rowSelectHandler = (id) => {
    if(!ids.includes(id)) {
      setIds([...ids,id]); 
    } else {
      console.log('i am here')
      setIds([...ids.slice(0,ids.findIndex(idx => idx === id)),...ids.slice(ids.findIndex(idx => idx === id) + 1 , ids.length)]);
    }
    console.log(id,ids);
  }

  return (
    <div className='app-wrapper'>
      <Filter onFilter={setQueryParams}></Filter>
      <Table savedData={savedData} ids={ids} data={testData.slice(rowNumber * pageNumber, rowNumber + (rowNumber * pageNumber))} onSort={setSortQueryParam} onRowSelect={rowSelectHandler}></Table>
      <Pagination pageNumber={pageNumber} rowNumber={rowNumber} onPageNumberChange={pageNumberHandler} onRowNumberChange={rowNumberHandler}></Pagination>
      <div className='background'></div>
    </div>
  );
}



export default App;
