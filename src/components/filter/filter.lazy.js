import React, { lazy, Suspense } from 'react';

const Lazyfilter = lazy(() => import('./filter'));

const filter = props => (
  <Suspense fallback={null}>
    <Lazyfilter {...props} />
  </Suspense>
);

export default filter;
