import React, { lazy, Suspense } from 'react';

const Lazytable = lazy(() => import('./table'));

const table = props => (
  <Suspense fallback={null}>
    <Lazytable {...props} />
  </Suspense>
);

export default table;
