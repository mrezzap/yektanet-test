import React, { lazy, Suspense } from 'react';

const Lazypagination = lazy(() => import('./pagination'));

const pagination = props => (
  <Suspense fallback={null}>
    <Lazypagination {...props} />
  </Suspense>
);

export default pagination;
