import React, {useState,useEffect} from 'react';
import PropTypes from 'prop-types';
import './pagination.scss';

function Pagination(props) {
  const sendPageNumber = (value) => {
    props.onPageNumberChange(value);
  }
  const sendRowsNumber = (value) => {
    props.onRowNumberChange(value);
  }
  return (
    <div className="pagination">
      <select onChange={(e) => sendRowsNumber(Number(e.target.value))}>
        <option value={25}>25</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
      </select>
      <input type='number' min="0" placeholder='page number' value={props.pageNumber} onChange={(e) => {sendPageNumber(Number(e.target.value))}}></input>
    </div>
  )
};

Pagination.propTypes = {
  pageNumber: PropTypes.number,
  rowNumber: PropTypes.number,
};

Pagination.defaultProps = {
  pageNumber: 0,
  rowNumber: 25
};

export default Pagination;
